﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace OYUN
{
    public partial class Secenek : Form
    {
        string renk = "1";
        string hizlan = "1";
        string hiz = "1";

        public Secenek()
        {
            InitializeComponent();
        }

        

        public void Kaydet()
        {
            XmlTextWriter yaz = new XmlTextWriter("secenekler.xml", System.Text.UTF8Encoding.UTF8);

            // Dosya yapısını hiyerarşik olarak oluşturarak okunabilirliği arttırır.
            yaz.Formatting = Formatting.Indented;

            //Satır oluşturduk
            yaz.WriteStartDocument();
            yaz.WriteStartElement("secenekler");
            yaz.WriteElementString("renk", renk);
            yaz.WriteElementString("hizlan", hizlan);
            yaz.WriteElementString("hiz", hiz);
            yaz.WriteEndElement();//secenekler sonu
            yaz.Close();
        }
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            renk = "1";
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            renk = "2";
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            renk = "3";
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            renk = "4";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Kaydet();
            this.Hide();
            Oyun form = new Oyun();
            form.ShowDialog();
        }

        private void radioButton6_CheckedChanged(object sender, EventArgs e)
        {
            groupBox3.Hide();
            hizlan = "0";
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)
        {
            groupBox3.Show();
            hizlan = "1";
        }

        private void radioButton8_CheckedChanged(object sender, EventArgs e)
        {
            hiz = "1";
        }

        private void radioButton7_CheckedChanged(object sender, EventArgs e)
        {
            hiz = "2";
        }

        private void radioButton9_CheckedChanged(object sender, EventArgs e)
        {
            hiz = "3";
        }

        private void Secenek_Load(object sender, EventArgs e)
        {

        }
    }
}
