﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;


namespace OYUN
{
    public partial class YuksekSkor : Form
    {

        //Verileri tutacağımız listeler

        ArrayList adlar = new ArrayList();
        ArrayList sureler = new ArrayList();

        string isim;
        string xmlKonum = "deneme.xml";
        string sure;

        
        public YuksekSkor()
        {
            InitializeComponent();
        }

        public void YuksekSkor_Load(object sender, EventArgs e)
        {
            Goster();
        }
        

       

        public void Okuma()
        {
            
            XmlTextReader XML = new XmlTextReader(xmlKonum);
            //tüm verileri çekme

            while (XML.Read())
            {
                if (XML.Name == "ad")
                {
                    adlar.Add(XML.ReadString());
                }
                if (XML.Name == "sure")
                {
                    sureler.Add(XML.ReadString());
                }
            }
            XML.Close();
        }

        public void Kontrol(int _sure)
        {
            Okuma();
            for(int i=0;i<5;i++)
            {
                if(_sure>Convert.ToInt32(sureler[i]))
                {
                    sure = _sure.ToString();
                    isim = Interaction.InputBox("İsminizi Giriniz");
                    Kaydet(i);
                    break;
                }
            }
        }


        public void Goster()
        {

            Okuma();
            //Adların labele yazılması
            label1.Text = adlar[0].ToString();
            label3.Text = adlar[1].ToString();
            label5.Text = adlar[2].ToString();
            label7.Text = adlar[3].ToString();
            label9.Text = adlar[4].ToString();

            //Surelerin labele yazılması
            label2.Text = sureler[0].ToString();
            label4.Text = sureler[1].ToString();
            label6.Text = sureler[2].ToString();
            label8.Text = sureler[3].ToString();
            label10.Text = sureler[4].ToString();
            
        }
        
        public void Kaydet(int i)
        {

                    sureler.Insert(i, sure);
                     adlar.Insert(i, isim);
                    Yaz();
        }



        public void Yaz()
        {
            Okuma();

            XmlTextWriter yaz = new XmlTextWriter("deneme.xml", System.Text.UTF8Encoding.UTF8);

            // Dosya yapısını hiyerarşik olarak oluşturarak okunabilirliği arttırır.
            yaz.Formatting = Formatting.Indented;
            

            //Satır oluşturduk
            yaz.WriteStartDocument();
            yaz.WriteStartElement("skorlar");

            for(int i=0;i<5;i++)
            {
                string ad = adlar[i].ToString();
                string saniye = sureler[i].ToString();
                yaz.WriteStartElement("skor");
                yaz.WriteElementString("ad", ad);
                yaz.WriteElementString("sure", saniye);
                yaz.WriteEndElement();//skor sonu
            }
            yaz.WriteEndElement();//skorlar sonu

            
            yaz.Close();


        }
    }
}
