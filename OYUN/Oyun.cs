﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace OYUN
{

    public partial class Oyun : Form
    {
        #region Degiskenler
        bool basladiMi = false;
        int sifir = 0;
        int saniye = 0;

        string renk;
        string hizlan;
        string hiz;

        int topHizi;

        //Varsayılan değerler
        Color arkaRenk = Color.Aqua;
        Color cubukRenk = Color.Black;


        int xHizi = 7;
        int yHizi = 12;
        #endregion



        public Oyun()
        {
            InitializeComponent();
        }

        //Form açıldığında
        private void Form1_Load(object sender, EventArgs e)
        {
            Oku();
            Uygula();
            formKontrol.Start();
            top.Hide();
            Cursor.Hide();
        }
        //Seceneklerden gelen XML in uygulanması
        public void Uygula()
        {
            if(renk=="1")
            {
                arkaRenk = Color.Red;
                cubukRenk = Color.White;
            }
            else if(renk=="2")
            {
                arkaRenk = Color.Blue;
                cubukRenk = Color.Yellow;
            }
            else if (renk == "3")
            {
                arkaRenk = Color.White;
                cubukRenk = Color.Black;
            }
            else if (renk == "4")
            {
                arkaRenk = Color.Green;
                cubukRenk = Color.Blue;
            }
        }
      

        //Form boyutunun ayarlanması
        private void formKontrol_Tick(object sender, EventArgs e)
        {
            panel1.Height = 40;
            panel1.Width = ClientSize.Width;
            cubuk.Top = ClientSize.Height-100;
        }
        //Fare hareketlerini izleme
        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
           cubuk.Left = e.X - 75;
            
            if(basladiMi==true)
            {
                top.Location = new Point(e.X, e.Y);
                top.Show();
                basladiMi = false;
                topKontrol.Start();
                zamanlama.Start();
            }
            
        }
        //Oyunu Başlatma
        private void Form1_Click(object sender, EventArgs e)
        {
            if(sifir==0)
            {
                basladiMi = true;
                sifir++;
            }
        }
        //Topun hareket kontrolü
        private void TopHareket()
        {
            if (this.ClientSize.Width <= top.Right)
            {
                xHizi = xHizi * -1;
            }
            else if (0 >= top.Left)
            {
                xHizi = xHizi * -1;
            }
            if (this.ClientSize.Height-panel1.Height <= top.Bottom)
            {
                yHizi = yHizi * -1;
            }
            else if (0 >= top.Top)
            {
                yHizi = yHizi * -1;
            }
            else if (top.Bottom >= cubuk.Top  && top.Right >= cubuk.Left && top.Left <= cubuk.Right)
            {
                yHizi = yHizi * -1;
                //Topun hızlanması

                if(hizlan=="1")
                {
                    if (xHizi <= 0)
                    {
                        xHizi -= topHizi;
                    }
                    else
                    {
                        xHizi += topHizi;
                    }
                    if (yHizi <= 0)
                    {
                        yHizi -= topHizi;
                    }
                    else
                    {
                        yHizi += topHizi;
                    }//Hız son
                }
                
                
            }
            //Kaybetme kontrol
            else if (top.Bottom >= cubuk.Top+20)
            {
                topKontrol.Stop();
                top.Hide();
                Kaybettiniz();
                sifir = 0;
            }

            top.Location = new System.Drawing.Point(top.Left + xHizi, top.Top + yHizi);
        }

        //Oyun Sona ermesi
        public void Kaybettiniz()
        {
            zamanlama.Stop();
            MessageBox.Show("Kaybettiniz...:(");
            YuksekSkor A = new YuksekSkor();
            A.Kontrol(saniye);
            A.Close();
            saniye = 0;
        }

        //Seceneklerden gelen XML in okunması
        public void Oku()
        {
            XmlTextReader XML = new XmlTextReader("secenekler.xml");
            //tüm verileri çekme

            while (XML.Read())
            {
                if (XML.Name == "renk")
                {
                    renk = XML.ReadString();
                }
                if (XML.Name == "hizlan")
                {
                    hizlan = XML.ReadString();
                }
                if(XML.Name=="hiz")
                {
                    hiz = XML.ReadString();
                }
            }

            topHizi = Convert.ToInt32(hiz);
         
            
            XML.Close();

        }

  

        private void topKontrol_Tick(object sender, EventArgs e)
        {
            TopHareket();
        }

        private void zamanlama_Tick(object sender, EventArgs e)
        {
            saniye++;
            label1.Text = saniye.ToString();
        }

        private void yuksekSkor_Click(object sender, EventArgs e)
        {
            YuksekSkor ySform = new YuksekSkor();
            ySform.ShowDialog();
        }

        private void cubuk_Paint(object sender, PaintEventArgs e)
        {
            cubuk.BackColor = cubukRenk;
            
        }

        private void Oyun_Paint(object sender, PaintEventArgs e)
        {
            BackColor = arkaRenk;
            top.BackColor = arkaRenk;
        }
        
    }
}
