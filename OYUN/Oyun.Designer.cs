﻿namespace OYUN
{
    partial class Oyun
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_yuksekSkor = new System.Windows.Forms.Button();
            this.cubuk = new System.Windows.Forms.PictureBox();
            this.top = new System.Windows.Forms.RadioButton();
            this.formKontrol = new System.Windows.Forms.Timer(this.components);
            this.topKontrol = new System.Windows.Forms.Timer(this.components);
            this.zamanlama = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cubuk)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btn_yuksekSkor);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 400);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(508, 40);
            this.panel1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(244, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(252, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Yeni Oyuna Başlamak İçin Tek Tıklayın";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.ForeColor = System.Drawing.Color.Tomato;
            this.label1.Location = new System.Drawing.Point(12, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 29);
            this.label1.TabIndex = 1;
            this.label1.Text = "0";
            // 
            // btn_yuksekSkor
            // 
            this.btn_yuksekSkor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_yuksekSkor.ForeColor = System.Drawing.Color.Tomato;
            this.btn_yuksekSkor.Location = new System.Drawing.Point(133, 0);
            this.btn_yuksekSkor.Name = "btn_yuksekSkor";
            this.btn_yuksekSkor.Size = new System.Drawing.Size(85, 40);
            this.btn_yuksekSkor.TabIndex = 0;
            this.btn_yuksekSkor.Text = "Yüksek Skorlar";
            this.btn_yuksekSkor.UseVisualStyleBackColor = true;
            this.btn_yuksekSkor.Click += new System.EventHandler(this.yuksekSkor_Click);
            // 
            // cubuk
            // 
            this.cubuk.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.cubuk.Location = new System.Drawing.Point(164, 364);
            this.cubuk.Name = "cubuk";
            this.cubuk.Size = new System.Drawing.Size(150, 20);
            this.cubuk.TabIndex = 1;
            this.cubuk.TabStop = false;
            this.cubuk.Paint += new System.Windows.Forms.PaintEventHandler(this.cubuk_Paint);
            // 
            // top
            // 
            this.top.AutoSize = true;
            this.top.BackColor = System.Drawing.SystemColors.Highlight;
            this.top.Location = new System.Drawing.Point(231, 84);
            this.top.Name = "top";
            this.top.Size = new System.Drawing.Size(14, 13);
            this.top.TabIndex = 2;
            this.top.TabStop = true;
            this.top.UseVisualStyleBackColor = false;
            // 
            // formKontrol
            // 
            this.formKontrol.Tick += new System.EventHandler(this.formKontrol_Tick);
            // 
            // topKontrol
            // 
            this.topKontrol.Tick += new System.EventHandler(this.topKontrol_Tick);
            // 
            // zamanlama
            // 
            this.zamanlama.Interval = 1000;
            this.zamanlama.Tick += new System.EventHandler(this.zamanlama_Tick);
            // 
            // Oyun
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Highlight;
            this.ClientSize = new System.Drawing.Size(508, 440);
            this.Controls.Add(this.top);
            this.Controls.Add(this.cubuk);
            this.Controls.Add(this.panel1);
            this.Name = "Oyun";
            this.Text = "Top Oyunu";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Click += new System.EventHandler(this.Form1_Click);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Oyun_Paint);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cubuk)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox cubuk;
        private System.Windows.Forms.RadioButton top;
        private System.Windows.Forms.Button btn_yuksekSkor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer formKontrol;
        private System.Windows.Forms.Timer topKontrol;
        private System.Windows.Forms.Timer zamanlama;
    }
}

